<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Notes App">
        <meta name="author" content="Heri Lesmana">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Notes App') }}</title>

        <title>Notes App</title>

        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
        <link rel="apple-touch-icon" href="{{ asset('images/favicon.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon.png') }}">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>
    <body>
        <div id="rootapp">
          <home></home>
          <div id="tf-service">
              <div class="container">

                  <div class="col-md-4">

                      <div class="media">
                        <div class="media-left media-middle">
                          <i class="fa fa-motorcycle"></i>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">Brand & Graphics Design</h4>
                          <p>Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                        </div>
                      </div>

                  </div>

                  <div class="col-md-4">

                      <div class="media">
                        <div class="media-left media-middle">
                          <i class="fa fa-gears"></i>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">Web Designer & Developer</h4>
                          <p>Cras sit amet nibh libero, in gravida nulla. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                        </div>
                      </div>

                  </div>

                  <div class="col-md-4">

                      <div class="media">
                        <div class="media-left media-middle">
                          <i class="fa fa-heartbeat"></i>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">Business Consultant</h4>
                          <p>Metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                        </div>
                      </div>

                  </div>

              </div>
          </div>

          <nav id="tf-footer">
              <div class="container">
                   <div class="pull-left">
                      <p>2017 © Notes App. All Rights Reserved. Zenius Code</p>
                  </div>
                  <div class="pull-right">
                      <ul class="social-media list-inline">
                          <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                          <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                          <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                          <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                          <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                          <li><a href="#"><span class="fa fa-behance"></span></a></li>
                      </ul>
                  </div>
              </div>
          </nav>

        </div>

        <script src="{{ asset('js/notesapp.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>

    </body>
</html>
